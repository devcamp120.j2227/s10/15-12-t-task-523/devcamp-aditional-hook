import ChildComponent1 from "./components/ChildComponent1";
import ChildComponent2 from "./components/ChildComponent2";
import Counter from "./components/Counter";
import { ExampleContext } from "./components/ExampleContext";
import UseCallbackComponent from "./components/UseCallbackComponent";
import UseMemoComponent from "./components/UseMemoComponent";

function App() {
  const initData = {
    firstValue:"first value",
    secondValue:"second value",
  }
  return (
    <div>
      <Counter />
      <ExampleContext.Provider value={initData}>
        <ChildComponent1/>
      </ExampleContext.Provider>
      <UseMemoComponent/>
      <UseCallbackComponent/>
    </div>
  );
}

export default App;
